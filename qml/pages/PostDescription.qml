import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property var title
    property var detail
    property var href
    property int margin: 20
    property string length
    property string duration
    property string date

    // The effective value will be restricted by ApplicationWindow.allowedOrientations

    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: flickable
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Open in browser")
                onClicked: {
                    console.log("href for browser: " + href)
                    Qt.openUrlExternally(href)
                }
             }
        }

        VerticalScrollDecorator { flickable: flickable }

        contentWidth: desccolumn.width
        contentHeight: desccolumn.height + desccolumn.spacing


        Column {
            id: desccolumn
            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: 'Post details'
            }
            Label {
                text: page.title
                wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeLarge
                color: Theme.highlightColor
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
            }

            Label {
                id: datacolumn
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                wrapMode: Text.WordWrap
                width: parent.with
                text: date + " | " + (length / 1024 / 1024).toFixed(1) + "MB" +
                      (duration != "" ? " | " + duration : "")
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
            }
            Label {
                id: detailcolumn
                width: parent.width
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: detail
                wrapMode: Text.WordWrap
                textFormat: Text.AutoText
                linkColor: Theme.highlightColor
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }
}
