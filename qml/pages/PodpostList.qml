import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page
    property var url
    property var link
    property var title
    property bool podpostslist: true

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        feedparserhandler.getEntries(url)
    }

    Connections {
        target: feedparserhandler
        ignoreUnknownSignals : true
        onCreatePodpostsList: {
            title = podtitle
            link = podlink
            podpostsModel.clear()
            for (var i = 0; i < pcdata.length; i++) {
                podpostsModel.append(pcdata[i]);
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable

    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator { }

        AppMenu { thispage: "Archive" }
        PrefAboutMenu { thelink: "https://gitlab.com/cy8aer/podqast/wikis/help-podlist"}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: page.title
                Column {
                    id: placeholder
                    width: Theme.itemSizeExtraSmall / 2
                }

                Column {
                    id: prefscol
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: placeholder.right
                    IconButton {
                       id: prefsicon
                        icon.source: "image://theme/icon-m-developer-mode"
                        onClicked: {
                            pageStack.push(Qt.resolvedUrl("PodcastSettings.qml"),
                                       { podtitle: title, url: url })
                        }
                    }
                }

            }
        }

        SilicaListView {
            id: archivepostlist
            anchors.top: archivetitle.bottom
            width: parent.width
            height: page.height - pdp.height - archivetitle.height
            section.property: 'section'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }

            ViewPlaceholder {
                enabled: podpostsModel.count == 0
                text: qsTr("Rendering")
                hintText: qsTr("Creating items")
                verticalOffset: - archivetitle.height
            }

            model: ListModel {
                id: podpostsModel
            }
            delegate: PodpostPostListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
