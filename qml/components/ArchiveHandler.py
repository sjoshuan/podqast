#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys

sys.path.append("/usr/share/harbour-podqast/python")

from podcast.archive import ArchiveFactory
from podcast.factory import Factory

def get_archive_posts(podurl=None):
    """
    Return a list of all archive posts
    """

    entries = []
    archive = ArchiveFactory().get_archive()

    for post in archive.get_podposts():
        entry = Factory().get_podpost(post)
        if podurl:
            if entry.podurl == podurl:
                entries.append(entry.get_data())
        else:
            if entry:
                entries.append(entry.get_data())
            else:
                pyotherside.send("ArchiveHandler: We have a none object")

    entries.sort(key = lambda r: r["adate"], reverse=True)
    pyotherside.send('createArchiveList', entries)

def get_archive_pod_data():
    """
    """

    entries = {}

    archive = ArchiveFactory().get_archive()

    for post in archive.get_podposts():
        entry = Factory().get_podpost(post)
        if entry.podurl in entries:
            entries[entry.podurl]["count"] += 1
        else:
            if entry.logo_url:
                logo_url = entry.logo_url
            else:
                logo_url = "../../images/podcast.png"
            entries[entry.podurl] = { "count": 1,
                                      "logo_url": logo_url }
            
    pyotherside.send('archivePodList', entries)

class ArchiveHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        self.bgthread2 = threading.Thread()
        self.bgthread2.start()
        pyotherside.atexit(self.doexit)

    def doexit(self):
        """
        On exit: we need to save ourself
        """

        ArchiveFactory().get_archive().save()

    def getarchiveposts(self, podurl=None):
        if self.bgthread2.is_alive():
            return
        if podurl:
            self.bgthread2 = threading.Thread(target=get_archive_posts, args=[podurl])
        else:
            self.bgthread2 = threading.Thread(target=get_archive_posts)
        self.bgthread2.start()


    def getarchivepoddata(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_archive_pod_data)
        self.bgthread.start()

archivehandler = ArchiveHandler()

