<p>
<h3>Utvecklare</h3>
  <a href="https://talk.maemo.org/member.php?u=33544">Cy8aer</a> (coding)<br>
  <a href="https://talk.maemo.org/member.php?u=31168">BluesLee</a> (testing)<br>
  <a href="http://www.einbilder.de">Daniel Noll</a> (painting)
<h4>Översättningar</h4>
  <a href="https://gitlab.com/eson">Åke Engelbrektson</a> (sv)
  <a href="https://gitlab.com/carmenfdezb">Carmen F. B.</a> (es)
</p>

<p>
<h3><a href="https://gitlab.com/cy8aer/podqast">PodQast</a> använder</h3>
<a href="https://github.com/kurtmckee/feedparser">feedparser</a>
av Kurt McKee och Mark Pilgrim<br>
<a href="http://gpodder.org/mygpoclient/">mygpoclient</a> av Thomas Perl och andra<br>
<a href="https://github.com/Alir3z4/html2text/">html2text</a> av Aaron Schwartz
och andra<br>
<a href="https://github.com/quodlibet/mutagen">mutagen</a> av Christoph Reiter och Joe Wreschnig
</p>

<p>
<h3>Sekretess</h3>
PodQast lagrar all information på enheten. För att hämta
  information om poddar, använder <a href="https://gpodder.net">gpodder.net</a>-tjänsten.
  Varje prenummererad podd-tjänst kan ha sin egen hantering av användardata.
  Du får själv inhämta information om detta, hos dessa tjänster.
</p>

<p>
<small>Version 2.2</small>
</p>

<p>Copyright (c) 2018 Thomas Renard</p>
