#!/usr/bin/env python3

import os
import pickle
import time

def cacher(cachefile, age):
    """
    a function which creates a decorator for caching function return
    to cachefile and use this cachefile if it is younger than age
    
    cachefile: file for caching the return data
    age: age of file in seconds to be renewed
    """

    def decorator(fn):
        def wrapped(*args, **kwargs):
            # if cache exists, load it to the current content
            if os.path.exists(cachefile):
                # what about the age?
                if time.time() - os.path.getmtime(cachefile) < age:
                    with open(cachefile, 'rb') as cachedhandle:
                        return pickle.load(cachedhandle)

            # execute the function with all arguments parsed

            res = fn(*args, **kwargs)

            # write file to cache
            with open(cachefile, 'wb') as cachedhandle:
                pickle.dump(res, cachedhandle)

            return res
        return wrapped
    return decorator                      # return the customized decorator

def memcacher(cachevar, cachename):
    """
    a function which creates a decorator for caching function return
    to cachefile and use this cachefile if it is younger than age
    
    cachefile: file for caching the return data
    age: age of file in seconds to be renewed
    """

    def decorator(fn):
        def wrapped(*args, **kwargs):
            # if cache exists, load it to the current content
            if cachename in cachevar:
                return cachevar[cachename]

            # execute the function with all arguments parsed

            res = fn(*args, **kwargs)

            # write file to cache
            cachevar[cachename] = res
            return res
        return wrapped
    return decorator                      # return the customized decorator
