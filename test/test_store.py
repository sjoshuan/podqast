"""
Test the Store class
"""

import sys
sys.path.append("../python")

import os

from podcast.store import Store

store = None
directory = "storetest"
try:
    os.stat(directory)
except:
    os.mkdir(directory)

def test_store_and_get():
    """
    Test the store function and do I get the data back?
    """

    len1 = len(os.listdir(directory))
    store = Store(directory)
    store.store("test1", 1)
    len2 = len(os.listdir(directory))
    
    element = store.get("test1")
    element2 = store.get("doesnotexist")

    assert len2 == len1 + 1
    assert element == 1
    assert not element2
    
def test_delete():
    store = Store(directory)
    len1 = len(os.listdir(directory))
    store.delete("test1")
    store.delete("idonotexist")           # no exception here!
    len2 = len(os.listdir(directory))

    assert len2 == len1 - 1
